-- MySQL dump 10.11
--
-- Host: localhost    Database: owasp10
-- ------------------------------------------------------
-- Server version	5.0.51a-3ubuntu5

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `accounts` (
  `cid` int(11) NOT NULL auto_increment,
  `username` text,
  `password` text,
  `mysignature` text,
  `is_admin` varchar(5) default NULL,
  PRIMARY KEY  (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'admin','adminpass','Monkey!','TRUE'),(2,'adrian','somepassword','Zombie Films Rock!','TRUE'),(3,'john','monkey','I like the smell of confunk','FALSE'),(4,'jeremy','password','d1373 1337 speak','FALSE'),(5,'bryce','password','I Love SANS','FALSE'),(6,'samurai','samurai','Carving Fools','FALSE'),(7,'jim','password','Jim Rome is Burning','FALSE'),(8,'bobby','password','Hank is my dad','FALSE'),(9,'simba','password','I am a cat','FALSE'),(10,'dreveil','password','Preparation H','FALSE'),(11,'scotty','password','Scotty Do','FALSE'),(12,'cal','password','Go Wildcats','FALSE'),(13,'john','password','Do the Duggie!','FALSE'),(14,'kevin','42','Doug Adams rocks','FALSE'),(15,'dave','set','Bet on S.E.T. FTW','FALSE'),(16,'ed','pentest','Commandline KungFu anyone?','FALSE');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs_table`
--

DROP TABLE IF EXISTS `blogs_table`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `blogs_table` (
  `cid` int(11) NOT NULL auto_increment,
  `blogger_name` text,
  `comment` text,
  `date` datetime default NULL,
  PRIMARY KEY  (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `blogs_table`
--

LOCK TABLES `blogs_table` WRITE;
/*!40000 ALTER TABLE `blogs_table` DISABLE KEYS */;
INSERT INTO `blogs_table` VALUES (1,'adrian','Well, I\'ve been working on this for a bit. Welcome to my crappy blog software. :)','2009-03-01 22:26:12'),(2,'adrian','Looks like I got a lot more work to do. Fun, Fun, Fun!!!','2009-03-01 22:26:54'),(3,'anonymous','An anonymous blog? Huh? ','2009-03-01 22:27:11'),(4,'ed','I love me some Netcat!!!','2009-03-01 22:27:48'),(5,'john','Listen to Pauldotcom!','2009-03-01 22:29:04'),(6,'jeremy','Why give users the ability to get to the unfiltered Internet? It\'s just asking for trouble. ','2009-03-01 22:29:49'),(7,'john','Chocolate is GOOD!!!','2009-03-01 22:30:06'),(8,'admin','Fear me, for I am ROOT!','2009-03-01 22:31:13'),(9,'dave','Social Engineering is woot-tastic','2009-03-01 22:31:13'),(10,'kevin','Read more Douglas Adams','2009-03-01 22:31:13'),(11,'kevin','You should take SANS SEC542','2009-03-01 22:31:13'),(12,'asprox','Fear me, for I am asprox!','2009-03-01 22:31:13');
/*!40000 ALTER TABLE `blogs_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `captured_data`
--

DROP TABLE IF EXISTS `captured_data`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `captured_data` (
  `data_id` int(11) NOT NULL auto_increment,
  `ip_address` text,
  `hostname` text,
  `port` text,
  `user_agent_string` text,
  `referrer` text,
  `data` text,
  `capture_date` datetime default NULL,
  PRIMARY KEY  (`data_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `captured_data`
--

LOCK TABLES `captured_data` WRITE;
/*!40000 ALTER TABLE `captured_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `captured_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_cards`
--

DROP TABLE IF EXISTS `credit_cards`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `credit_cards` (
  `ccid` int(11) NOT NULL auto_increment,
  `ccnumber` text,
  `ccv` text,
  `expiration` date default NULL,
  PRIMARY KEY  (`ccid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `credit_cards`
--

LOCK TABLES `credit_cards` WRITE;
/*!40000 ALTER TABLE `credit_cards` DISABLE KEYS */;
INSERT INTO `credit_cards` VALUES (1,'4444111122223333','745','2012-03-01'),(2,'7746536337776330','722','2015-04-01'),(3,'8242325748474749','461','2016-03-01'),(4,'7725653200487633','230','2017-06-01'),(5,'1234567812345678','627','2018-11-01');
/*!40000 ALTER TABLE `credit_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hitlog`
--

DROP TABLE IF EXISTS `hitlog`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `hitlog` (
  `cid` int(11) NOT NULL auto_increment,
  `hostname` text,
  `ip` text,
  `browser` text,
  `referer` text,
  `date` datetime default NULL,
  PRIMARY KEY  (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `hitlog`
--

LOCK TABLES `hitlog` WRITE;
/*!40000 ALTER TABLE `hitlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `hitlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pen_test_tools`
--

DROP TABLE IF EXISTS `pen_test_tools`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pen_test_tools` (
  `tool_id` int(11) NOT NULL auto_increment,
  `tool_name` text,
  `phase_to_use` text,
  `tool_type` text,
  `comment` text,
  PRIMARY KEY  (`tool_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pen_test_tools`
--

LOCK TABLES `pen_test_tools` WRITE;
/*!40000 ALTER TABLE `pen_test_tools` DISABLE KEYS */;
INSERT INTO `pen_test_tools` VALUES (1,'WebSecurify','Discovery','Scanner','Can capture screenshots automatically'),(2,'Grendel-Scan','Discovery','Scanner','Has interactive-mode. Lots plug-ins. Includes Nikto. May not spider JS menus well.'),(3,'Skipfish','Discovery','Scanner','Agressive. Fast. Uses wordlists to brute force directories.'),(4,'w3af','Discovery','Scanner','GUI simple to use. Can call sqlmap. Allows scan packages to be saved in profiles. Provides evasion, discovery, brute force, vulneraility assessment (audit), exploitation, pattern matching (grep).'),(5,'Burp-Suite','Discovery','Scanner','GUI simple to use. Provides highly configurable manual scan assistence with productivity enhancements.'),(6,'Netsparker Community Edition','Discovery','Scanner','Excellent spider abilities and reporting. GUI driven. Runs on Windows. Good at SQLi and XSS detection. From Mavituna Security. Professional version available for purchase.'),(7,'NeXpose','Discovery','Scanner','GUI driven. Runs on Windows. From Rapid7. Professional version available for purchase. Updates automatically. Requires large amounts of memory.'),(8,'Hailstorm','Discovery','Scanner','From Cenzic. Professional version requires dedicated staff, multiple dediciated servers, professional pen-tester to analyze results, and very large license fee. Extensive scanning ability. Very large vulnerability database. Highly configurable. Excellent reporting. Can scan entire networks of web applications. Extremely expensive. Requires large amounts of memory.'),(9,'Tamper Data','Discovery','Interception Proxy','Firefox add-on. Easy to use. Tampers with POST parameters and HTTP Headers. Does not tamper with URL query parameters. Requires manual browsing.'),(10,'DirBuster','Discovery','Fuzzer','OWASP tool. Fuzzes directory names to brute force directories.'),(11,'SQL Inject Me','Discovery','Fuzzer','Firefox add-on. Attempts common strings which elicit XSS responses. Not compatible with Firefox 8.0.'),(12,'XSS Me','Discovery','Fuzzer','Firefox add-on. Attempts common strings which elicit responses from databases when SQL injection is present. Not compatible with Firefox 8.0.'),(13,'GreaseMonkey','Discovery','Browser Manipulation Tool','Firefox add-on. Allows the user to inject JavaScripts and change page.'),(14,'NSLookup','Reconnaissance','DNS Server Query Tool','DNS query tool can query DNS name or reverse lookup on IP. Set debug for better output. Premiere tool on Windows but Linux perfers Dig. DNS traffic generally over UDP 53 unless response long then over TCP 53. Online version combined with anonymous proxy or TOR network may be prefered for stealth.'),(15,'Whois','Reconnaissance','Domain name lookup service','Whois is available in Linux naitvely and Windows as a Sysinternals download plus online. Whois can lookup the registrar of a domain and the IP block associated. An online version is http://network-tools.com/'),(16,'Dig','Reconnaissance','DNS Server Query Tool','The Domain Information Groper is prefered on Linux over NSLookup and provides more information natively. NSLookup must be in debug mode to give similar output. DIG can perform zone transfers if the DNS server allows transfers.'),(17,'Fierce Domain Scanner','Reconnaissance','DNS Server Query Tool','Powerful DNS scan tool. FDS is a Perl program which scans and reverse scans a domain plus scans IPs within the same block to look for neighoring machines. Available in the Samurai and Backtrack distributions plus http://ha.ckers.org/fierce/'),(18,'host','Reconnaissance','DNS Server Query Tool','A simple DNS lookup tool included with BIND. The tool is a friendly and capible command line tool with excellent documentation. Does not posess the automation of FDS.'),(19,'zaproxy','Reconnaissance','Interception Proxy','OWASP Zed Attack Proxy. An interception proxy that can also passively or actively scan applications as well as perform brute-forcing. Similar to Burp-Suite without the disadvantage of requiring a costly license.'),(20,'Google intitle','Discovery','Search Engine','intitle and site directives allow directory discovery. GHDB available to provide hints. See Hackers for Charity site.');
/*!40000 ALTER TABLE `pen_test_tools` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-09-23 12:03:15
